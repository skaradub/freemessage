package com.skaradub.freemessages;

import java.sql.DataTruncation;
import java.sql.Timestamp;
import java.util.Date;

import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.converter.PropertyConverter;

@Entity
public class DBChatContent {

    @Id
    long id;

    long chat_id ;
    String user;
    String pictures;
    String text;
    Integer status;
    Integer be_id;
    private Date timestamp;

    public DBChatContent(long id, String user, String text, String pictures, Integer status, Integer be_id, Date timestamp) {
        this.id = id;
        this.user= user;
        this.pictures = pictures;
        this.text = text;
        this.be_id = be_id;
        this.status = status;
        this.timestamp = timestamp;
    }

    public DBChatContent() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getChat_id() {
        return chat_id;
    }

    public void setChat_id(long chat_id) {
        this.chat_id = chat_id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getBe_id() {
        return be_id;
    }

    public void setBe_id(Integer be_id) {
        this.be_id = be_id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}