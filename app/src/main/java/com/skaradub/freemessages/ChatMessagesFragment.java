package com.skaradub.freemessages;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

import static com.skaradub.freemessages.App.getApplication;

public class ChatMessagesFragment extends ListFragment {

    private DBChat anyChat;
    private List<DBChatContent> chatMessages;
    BoxStore mboxStore = ((App) getApplication()).getBoxStore();
    Box<DBChat> chats = mboxStore.boxFor(DBChat.class);
    Box<DBChatContent> chatContent = mboxStore.boxFor(DBChatContent.class);
    private List<DBChatContent> ChatMessageArray;
    private Query<DBChatContent> chatsQuery;
    ArrayList<DBChatContent> ff;
    private ChatMessagesAdapter ad1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long position = (long) getArguments().getLong("chatID");
        anyChat  = chats.get(position);
        chatMessages = chatContent.query().equal(DBChatContent_.chat_id, position).build().find();
        chatsQuery = chatContent.query().equal(DBChatContent_.chat_id,position).build();
        ChatMessageArray = chatsQuery.find();
        ff = (ArrayList<DBChatContent>) ChatMessageArray;
        ad1 = new ChatMessagesAdapter(ff);
        setListAdapter(ad1);
    }

    public static ChatMessagesFragment newInstance(long pos) {

        Bundle args = new Bundle();
        args.putLong("chatID", pos);
        ChatMessagesFragment fragment = new ChatMessagesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private  class ChatMessagesAdapter extends ArrayAdapter<DBChatContent> {
        private List <DBChatContent> dataset;

        public ChatMessagesAdapter(ArrayList<DBChatContent> allChats){

            super(getActivity(),R.layout.chat_item, allChats);
        }
        @Override
        public View getView(int whichItem, View view, ViewGroup viewGroup){
            if (view==null){
                LayoutInflater inflater = (LayoutInflater) getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.chat_message_item,null);
            }

            DBChatContent tempChat = getItem(whichItem);
            TextView textmess = (TextView) view.findViewById(R.id.text_message);

            textmess.setText("" + tempChat.getId());
            return view;
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach(){
        super.onDetach();

    }

}