package com.skaradub.freemessages;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.app.Activity;

import java.util.Date;

import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.skaradub.freemessages.App.getApplication;

public class ChatContent extends AppCompatActivity{
    public BoxStore mboxStore;
    public Box<DBChatContent> chatContent;
    public long pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mboxStore =  ((App) getApplication()).getBoxStore();
        chatContent = mboxStore.boxFor(DBChatContent.class);
        setContentView(R.layout.activity_chatting);
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            pos= 1;
        } else {
            pos= extras.getLong("chatID");
        }

        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();
        Fragment fOld = fManager.findFragmentById(R.id.listFragmentHolder);
        Fragment fNew = ChatMessagesFragment.newInstance(pos);

        if (fOld != null) {
            fTransaction.remove(fOld);
        }
        fTransaction.add(R.id.listFragmentHolder, fNew);
        fTransaction.commit();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Button for new message
        Button sendMessButt = (Button) findViewById(R.id.addmessbutt);
        final EditText Message = (EditText) findViewById(R.id.inputtext);

        sendMessButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBChatContent gg = new DBChatContent();
                gg.setChat_id(pos);
                gg.setText(Message.getText().toString());
                gg.setTimestamp(new Date());
                long newid = chatContent.put(gg);

            }
        });

    }


}
