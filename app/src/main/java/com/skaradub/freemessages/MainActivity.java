package com.skaradub.freemessages;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements  PressCommunication, DialogEditChat.EditDialogListener{

    Intent mServiceIntent;
    SyncChatService syncService;
    Context ctx;
    public Context getCtx() {
        return ctx;
    }

    @Override
    public void onListItemSelected(long pos) {
        if (findViewById(R.id.LastMessagesHolder) == null) {
            Intent i = new Intent(this, ChatContent.class);
            i.putExtra("chatID",pos);
            startActivity(i);
        }
        else {
            FragmentManager fManager = getSupportFragmentManager();
            FragmentTransaction fTransaction = fManager.beginTransaction();
            Fragment fOld = fManager.findFragmentById(R.id.LastMessagesHolder);
            Fragment fNew = ChatMessagesFragment.newInstance(pos);

            if (fOld != null) {
                fTransaction.remove(fOld);
            }
            fTransaction.add(R.id.LastMessagesHolder, fNew);
            fTransaction.commit();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.main_activity);

        FragmentManager fManager = getSupportFragmentManager();
        Fragment frag = fManager.findFragmentById(R.id.listFragmentHolder);
        if (frag == null) {
            frag = new ChatListFragment();
            fManager.beginTransaction()
            .add(R.id.listFragmentHolder,frag)
                    .commit();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogEditChat newChat = new DialogEditChat().newInstance();
                newChat.show(getFragmentManager(),"1202");
            }
        });

        // background sync of chats

        syncService = new SyncChatService(getCtx());
        mServiceIntent = new Intent(getCtx(), syncService.getClass());
        if (!isMyServiceRunning(syncService.getClass())) {
            startService(mServiceIntent);
        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    @Override
    protected void onDestroy() {
        stopService(mServiceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }

    @Override
    public void onFinishEditDialog(long id) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        ChatListFragment fragment =   (ChatListFragment) fragmentManager.findFragmentById(R.id.listFragmentHolder);
        fragment.refresh(id);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //no
        // inspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
