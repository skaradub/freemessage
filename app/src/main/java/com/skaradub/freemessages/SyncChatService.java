package com.skaradub.freemessages;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;
import retrofit2.Response;

public class SyncChatService extends Service {
    public int counter=0;
    Context context;
    private DBChat anyChat;
    private List<DBChatContent> chatMessages;
    Box<DBChat> chats;
    private List<DBChat> ChatlistArray;
    private Query<DBChat> chatsQuery;
    Response<List<ApiModelChatMessage>> response;

    public SyncChatService(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
    }

    public SyncChatService() {
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        context = getApplicationContext();
        BoxStore mboxStore = ((App) context).getBoxStore();
        chats = mboxStore.boxFor(DBChat.class);
        startTimer();
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent(".RestartSyncService");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 5000, 5000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                // Here we place the code to sync
                chatsQuery = chats.query().build();
                ChatlistArray = chatsQuery.find();
                int chatsize = ChatlistArray.size();
                for (int t=0;t < ChatlistArray.size(); t++) {
                    DBChat l = ChatlistArray.get(t);
                    Log.i("in timer", "chats: " + (l.getAddress()));
                    boolean isError = false;
                    try {
                        response = App.getApi().getChatMessages(l.getAddress() + "/chatmessages/" + l.getKey() + "/").execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                        isError = true;
                    }
                    if (!isError){
                        if (response.isSuccessful()) {
                            for (ApiModelChatMessage ap : response.body()) {
                                Log.i("in timer", "chats_body: " + (ap.getMessage()));

                            }
                        }
                    }

                    Log.i("in timer2", "in timer ++++  " + (counter++));
                }

            }
        };
    }

    /**
     * not needed indeed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}