package com.skaradub.freemessages;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.skaradub.freemessages.App.getApplication;


public class DialogEditChat extends DialogFragment{
    long mNum;
    BoxStore mboxStore = ((App) getApplication()).getBoxStore();
    Box<DBChat> chats = mboxStore.boxFor(DBChat.class);
    DBChat anyChat;

    public interface EditDialogListener {
        void onFinishEditDialog(long id);
    }

    static DialogEditChat newInstance(long num) {
        DialogEditChat f = new DialogEditChat();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putLong("num", num);
        f.setArguments(args);

        return f;
    }

    static DialogEditChat newInstance() {
        DialogEditChat f = new DialogEditChat();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putLong("num", 0);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments().getLong("num");
    }

    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        //Creation of dialog and xml for it
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_chat, null);
        // fields
        final EditText editName = (EditText) dialogView.findViewById(R.id.name);
        final EditText editAddress = (EditText) dialogView.findViewById(R.id.address);
        final EditText editKey = (EditText) dialogView.findViewById(R.id.key);
        Button btnSave = (Button) dialogView.findViewById(R.id.Savebtn);
        Button btnCancel = (Button) dialogView.findViewById(R.id.Cancelbtn);
        if (mNum > 0) {
            anyChat = chats.query().equal(DBChat_.id, mNum).build().findFirst();
            editAddress.setText(anyChat.getAddress());
            editName.setText(anyChat.getName());
            editKey.setText(anyChat.getKey());
        }
        btnCancel.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnSave.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String mAddress = editAddress.getText().toString();
                String mKey = editKey.getText().toString();
                String mName = editName.getText().toString();
                if (mNum == 0) {

                    DBChat gg = new DBChat(0, mName, mAddress, mKey);
                    long newid = chats.put(gg);
                    EditDialogListener activity = (EditDialogListener) getActivity();
                    activity.onFinishEditDialog(newid);
                }
                else {
                    DBChat gg = chats.get(mNum);
                    gg.setId(mNum);
                    gg.setAddress(mAddress);
                    gg.setKey(mKey);
                    gg.setName(mName);
                    chats.put(gg);
                    EditDialogListener activity = (EditDialogListener) getActivity();
                    activity.onFinishEditDialog(gg.getId());
                }
                dismiss();
            }
        });

        // Show dialog
        builder.setView(dialogView).setMessage("Add|Edit Chat");
        return  builder.create();

    }

}
