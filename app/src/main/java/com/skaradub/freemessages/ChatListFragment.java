package com.skaradub.freemessages;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

import static com.skaradub.freemessages.App.*;

public class ChatListFragment extends android.support.v4.app.ListFragment implements  DialogEditChat.EditDialogListener{
    private PressCommunication CL_Presscoms;
    private List<DBChat> ChatlistArray;
    private ChatListAdapter ad1;
    private Query<DBChat> chatsQuery;
    ArrayList<DBChat> ff;
    BoxStore mboxStore = ((App) getApplication()).getBoxStore();
    Box<DBChat> chats = mboxStore.boxFor(DBChat.class);

    public void onFinishEditDialog(long id) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        chatsQuery = chats.query().build();
        ChatlistArray = chatsQuery.find();

        ff = (ArrayList<DBChat>) ChatlistArray;
        ad1 = new ChatListAdapter(ff);
        setListAdapter(ad1);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(getListView());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v,
                                    final ContextMenu.ContextMenuInfo menuInfo){
        menu.clear();
        super.onCreateContextMenu(menu, v, menuInfo);
        final MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_chats, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        //get position chosen from list
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int pos = info.position;
        final long ed_id = ChatlistArray.get(pos).getId();
        switch (item.getItemId()) {
            case R.id.change_chat:
                DialogEditChat newChat2 = new DialogEditChat().newInstance(ed_id);
                newChat2.show(this.getActivity().getFragmentManager(), "1203");
                break;
            case R.id.delete_chat:
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Confirm deletion");
                alertDialog.setMessage("You're going to delete chat");

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ad1.remove(ad1.getItem(pos));
                                chats.remove(chats.get(ed_id));
                                ad1.notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
        }
        return true;
    }

    public void refresh(long id){

        DBChat newChat =  chats.query().equal(DBChat_.id,id).build().findFirst();
        boolean found= false;
        int i=0;
        for (i=0;i<=ad1.getCount()-1;i++){
            if (ad1.getItem(i).getId() == id) {
                found = true;
                break;
            }
        }
        if (found) {
            ad1.getItem(i).setName(chats.get(id).getName());
            ad1.getItem(i).setAddress(chats.get(id).getAddress());
            ad1.getItem(i).setKey(chats.get(id).getKey());
            ad1.notifyDataSetChanged();
        }
        else {
            ad1.add(newChat);
            ad1.notifyDataSetChanged();
        }
        }

    private  class ChatListAdapter extends ArrayAdapter<DBChat> {
        private List <DBChat> dataset;

        public ChatListAdapter(ArrayList<DBChat> allChats){

            super(getActivity(),R.layout.chat_item, allChats);
        }
        @Override
        public View getView(int whichItem, View view, ViewGroup viewGroup){
            if (view==null){
                LayoutInflater inflater = (LayoutInflater) getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.chat_item,null);
            }

            DBChat tempChat = getItem(whichItem);
            TextView name = (TextView) view.findViewById(R.id.chat_name);
            TextView usercount = (TextView) view.findViewById(R.id.usercount);

            name.setText(tempChat.getName());
            usercount.setText("" + tempChat.getId());
            return view;
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        CL_Presscoms = (PressCommunication) activity;
    }

    @Override
    public void onDetach(){
        super.onDetach();
        CL_Presscoms = null;
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        DBChat pressedChat = ChatlistArray.get(position);
        CL_Presscoms.onListItemSelected(pressedChat.getId());
    }

}
