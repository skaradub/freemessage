package com.skaradub.freemessages;

/**
 * Created by yury on 22.12.17.
 */
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface GetChatsApi {
    @GET
    Call<List<ApiModelChat>> getChats(@Url String url);

    @GET
    Call<List<ApiModelChatMessage>> getChatMessages(@Url String url);
}