package com.skaradub.freemessages;

import android.app.Application;
import android.content.Context;

import io.objectbox.BoxStore;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    private static Application sApplication;
    private BoxStore boxStore;

    private static GetChatsApi GetChatsApi;
    private Retrofit retrofit;

    public static Application getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        //DB
        boxStore = MyObjectBox.builder().androidContext(App.getContext()).build();


        retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8000/") //Bаse URL
                .addConverterFactory(GsonConverterFactory.create()) //Create
                .build();
        GetChatsApi = retrofit.create(GetChatsApi.class); //get Chats

    }

    public static GetChatsApi getApi() {
        return GetChatsApi;
    }

    public BoxStore getBoxStore() {
        return boxStore;
    }

}